# image-checker

## Description
Bookmarklet con Vue JS / Busca imagenes con mismo nombre en diferentes viewports y su _2x.

## Preview
Click [aqui](https://zen-sinoussi-19c38e.netlify.app/) para preview.

## Install
Para instalar en tu navegador, solo agrega un nuevo marcador a tu barra de favoritos y pone el siguiente codigo en el campo URL.

`javascript:(function()%7Bvar app%3Ddocument.createElement("div")%3Bapp.setAttribute("id","app"),document.body.appendChild(app)%3Bvar a%3Ddocument.createElement("script")%3Ba.type%3D"text/javascript",a.src%3D"https://zen-sinoussi-19c38e.netlify.app/chunk-vendors.js",document.head.appendChild(a)%3Bvar b%3Ddocument.createElement("script")%3Bb.type%3D"text/javascript",b.src%3D"https://zen-sinoussi-19c38e.netlify.app/app.js",document.head.appendChild(b)%3Bvar c%3Ddocument.createElement("link")%3Bc.rel%3D"stylesheet",c.type%3D"text/css",c.href%3D"https://zen-sinoussi-19c38e.netlify.app/app.css",document.head.appendChild(c)%3Bvar d%3Ddocument.createElement("link")%3Bd.rel%3D"stylesheet",d.type%3D"text/css",d.href%3D"https://zen-sinoussi-19c38e.netlify.app/chunk-vendors.css",document.head.appendChild(d)%7D)()%3B`


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
